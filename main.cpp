//
// Created by pmeziere on 10/11/22.
//

#include <iostream>
#include <fstream>
#include "Code/Helpers.hpp"
#include "Code/Read.hpp"

int main(int argc, char *argv[]){

    Args args(argc, argv);


    std::string filename = args.getString();

    if(args.argsNumber({1})){
        Read read(filename,Read::Type::FULL);
        return true;
    }
    if(args.argsNumber({2})){
        if(args.getStringEqual("dozen")){
            Read read(filename,Read::Type::DOZEN);
        }else{
            Read read(filename,Read::Type::SIMPLE);
        }
        return true;
    }
    if(args.argsNumber({3})){
        int theta = args.getInt();
        int phi = args.getInt();
        Read read(filename,Read::Type::PRECISE,theta,phi);
        return true;
    }

    Log(logNoLabel) << "";
    Log(logInfo) << "How to use it : ";
    Log(logInfo) << "./read filename                 < Print the whole file ";
    Log(logInfo) << "./read filename simple/dozen    < Print the number of SH coefficients for each input direction";
    Log(logInfo) << "./read filename theta phi       < Print the SH coefficients for one input direction";
    Log(logNoLabel) << "";

    return 0;

}