  
# SHmaterials

 This git repository contains a SH projection database of hundreds of materials from the [MERL](https://www.merl.com/brdf/) and [RGL](https://rgl.epfl.ch/materials) databases. This repository serves as an appendix to my thesis called: [Real spherical harmonics for lighting simulation and real-time rendering](https://pierremezieres.github.io/).    
    
## Mathematical background 

### SH projection of a BRDF    

 A BRDF is a function of the two directions $\omega_i$ and $\omega_o$, yielding a 4-dimensional function. When evaluating $F$, the direction $\omega_o$ is fixed; hence, the BRDF evaluation corresponds to a 2-dimensional function projected on SH. The SH coefficients are tabulated according to discrete samples of the direction $\omega_o = (\theta_o, \phi_o)$.    
    
$$ \mathrm{Tab}[\theta_o, \phi_o] = \mathbf{F}_{\omega_o} $$    
    
where $\mathbf{F}_{\omega_o}$ is the SH coefficients vector of $\mathbf{F}$ computed for the output direction $\omega_o$. The dabase is created with a sampling step of one degree for $\theta_o$ and $\phi_o$.    
    
### Isotropic material    
 We store isotropic material in a one-dimensional array. Isotropic materials depend on the $\Delta\phi=\left\lvert\phi_i-\phi_o\right\rvert$ rather than the actual values of $\phi_i$ and $\phi_o$. Hence, by using the isotropic parametrization of $F$:    
    
$$ F_{iso}(\theta_o, \theta_i, \Delta\phi) = F(\theta_o, 0, \theta_i, \Delta\phi) $$    
    
we drop the explicit dependence on $\phi_o$ and only discretize $\theta_o$. This way, the storage of isotropic material drops down to $90$ samples    
    
$$ \mathrm{Tab}[\theta_o] = \mathbf{F}_{\omega_o} \quad\text{ with }\quad\omega_o=(\theta_o, 0)$$     
    
### Study on isotropic materials    
 We produced a [Study](https://docs.google.com/spreadsheets/d/1TpuuFFhH23ClEuvtEHw2paSQTsRz3aNwSphbl2Ni0Z8/edit?usp=sharing) to analyze the number of SH bands needed to capture the material according to a percentage of signal energy capture.  In order to simplify the data visualization, this study is limited to isotropic material since their SH projection depends only on $\omega_o$.    
    
## Database    
 The SH projection of the materials from the [MERL](https://www.merl.com/brdf/) and [RGL](https://rgl.epfl.ch/materials) databases are available in the directory `Database`.  The SH projection was computed on 20 SH bands (at most) for each material and store in binary files. The code to read these binary files is in the file `Code/Read.cpp`. The materials are projected with (`withTheta`) or without (`withoutTheta`) the cosine of the  [rendering equation](https://en.wikipedia.org/wiki/Rendering_equation) ($\omega_i \cdot \mathbf{n}$). We also projected the MERL materials fitted on the GGX model (according to [Mickael Ribardière](https://ribardiere.pages.xlim.fr/projects/eg_2017/)'s data).  

This database only gives the SH projection for isotropic materials because anisotropic materials take up a lot of memory space. Nevertheless, you can compute yourself their SH projection  (see below  :arrow_down:).

  ## Read the SH projection   
The code to read the binary files of the SH projection is in the file `Code/Read.cpp`.     
  
 We have also added a sample program (to be compiled with `make`) to read the projection files `./read`. You can use it like this:  
``` 
./read filename                ->  Print the whole file  
./read filename simple/dozen   ->  Print the number of SH coefficients for each input direction  
./read filename theta phi      ->  Print the SH coefficients for one input direction  
```    
 ## Code of the SH projection and other    
    
 The code to compute the SH projection is available on this [repository](https://gitlab.com/PierreMezieres/LightingTools). You will also find other codes related to the simulation of lighting with SH.
