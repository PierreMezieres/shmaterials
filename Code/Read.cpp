//
// Created by pmeziere on 10/11/22.
//

#include <regex>
#include "Read.hpp"


LoadShMat::LoadShMat(std::string filename) {

    f = fopen(filename.data(), "rb");
    if (!f){
        Log(logError) << "Can't read file: " << filename;
        return;
    }

    if(fread(&m_version, sizeof(int), 1, f)!=1) return;

    Log(logDebug) << "Version : " << m_version;

    switch(m_version){
        case 1: read_v1(); break;
        case 2: read_v2(); break;
        default: Log(logError) << "[LoadShMat] This version does not exist ...";
    }
}

void LoadShMat::read_v1(){

    Vector3 c;

    if(fread(&m_theta, sizeof(int), 1, f)!=1) return;
    if(fread(&m_phi, sizeof(int), 1, f)!=1) return;


    while(true){
        int numberCoeffs;
        if(fread(&numberCoeffs, sizeof(int), 1, f)!=1) return;


        std::vector<Vector3> normalizationC;
        int bands = std::sqrt(numberCoeffs);
        for(int i=0;i<bands;i++){
            normalizationC.push_back(Vector3(1));
        }

        std::vector<Vector3> C;
        for(int i=0;i<numberCoeffs;i++){
            fread(&c, sizeof(float), 3, f);
            C.push_back(c);
        }

        coeffs.push_back({C,normalizationC,numberCoeffs});

    }
}

void LoadShMat::read_v2(){

    Vector3 c;

    if(fread(&m_theta, sizeof(int), 1, f)!=1) return;
    if(fread(&m_phi, sizeof(int), 1, f)!=1) return;


    while(true){
        int numberCoeffs;
        if(fread(&numberCoeffs, sizeof(int), 1, f)!=1) return;

        std::vector<Vector3> normalizationC;
        int bands = std::sqrt(numberCoeffs);
        for(int i=0;i<bands;i++){
            fread(&c, sizeof(float), 3, f);
            normalizationC.push_back(c);
        }

        std::vector<Vector3> C;
        for(int i=0;i<numberCoeffs;i++){
            fread(&c, sizeof(float), 3, f);
            C.push_back(c);
        }

        coeffs.push_back({C,normalizationC,numberCoeffs});

    }
}



Read::Read(std::string filename, Type type, int theta, int phi):
        m_type{type}, m_theta{theta}, m_phi{phi} {

    Log(logInfo) << "Read file: " << filename;


    if (std::regex_match(filename, std::regex(".*\\.shmat")) ||
        std::regex_match(filename, std::regex(".*\\.hshmat"))) {
        shmat(filename);
    } else {
        Log(logError) << "We can't read this kind of file ...";
    }
}

void Read::shmat(std::string filename) {

    LoadShMat shmat(filename);

    Log(logDebug) << " Theta number:" << shmat.m_theta << " Phi number:" << shmat.m_phi;
    Log(logNoLabel) << "";

    int i=0;
    for(int angleP = 0; angleP < shmat.m_phi; angleP ++) {
        for(int angleT = 0; angleT < shmat.m_theta; angleT ++){
            if (m_type == FULL) Log(logNoLabel) << "";
            int numberCoeff = shmat.coeffs[i].numberCoeffs;

            if(m_type==FULL || m_type==SIMPLE || (m_type==DOZEN && angleT%10==0) || (m_type==PRECISE && m_theta==angleT && m_phi==angleP))
                Log(logDebug) << "Theta : " << angleT << "  Phi : " << angleP << "  #coeffs: " << numberCoeff << " band:" << sqrt(numberCoeff)-1;

            if(shmat.m_version==2){
                if(m_type==FULL || (m_type==PRECISE && m_theta==angleT && m_phi==angleP))
                    Log(logInfo) << "Normalization coeffs";
                for(int j=0;j<std::sqrt(numberCoeff);j++) {
                    Vector3 c = shmat.coeffs[i].normalizationCoeffs[j];
                    if(m_type==FULL || (m_type==PRECISE && m_theta==angleT && m_phi==angleP))
                        Log(logInfo) << j <<"  r=" << c.x << " g=" << c.y << " b=" << c.z;
                }
            }

            if(m_type==FULL || (m_type==PRECISE && m_theta==angleT && m_phi==angleP))
                Log(logInfo) << "Coeffs";
            for(int j=0;j<numberCoeff;j++) {
                Vector3 c = shmat.coeffs[i].coeffs[j];
                if(m_type==FULL || (m_type==PRECISE && m_theta==angleT && m_phi==angleP))
                    Log(logInfo) << j <<"  r=" << c.x << " g=" << c.y << " b=" << c.z;
            }
            i++;
        }
    }
}