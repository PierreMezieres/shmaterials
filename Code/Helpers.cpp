//
// Created by pmeziere on 10/11/22.
//

#include "Helpers.hpp"

Args::Args(int argc, char **argv): m_argc{argc}, m_argv{argv}{
    srand(time(NULL));
}

std::string Args::getString(std::string def){
    if(m_argc<=arg) return def;
    arg++;
    return m_argv[arg-1];
}

bool Args::getStringEqual(std::string s){
    if(m_argc<=arg) return false;
    arg++;
    return !strcmp(s.data(),m_argv[arg-1]);
}

float Args::getFloat(float def){
    if(m_argc<=arg) return def;
    arg++;
    return std::atof(m_argv[arg-1]);
}

int Args::getInt(int def){
    if(m_argc<=arg) return def;
    arg++;
    return std::atoi(m_argv[arg-1]);
}

Vector3 Args::getVector3(Vector3 vec){
    float x = getFloat(vec.x);
    float y = getFloat(vec.y);
    float z = getFloat(vec.z);
    return Vector3(x,y,z);

}

bool Args::command(std::string com){
    if(m_argc>1 && !strcmp(m_argv[1],com.data())) return true;
    return false;
}

bool Args::argsNumber(std::vector<int> args){

    for ( int arg: args ){
        if(m_argc-1 == arg) return true;
    }
    return false;
}