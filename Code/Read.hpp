//
// Created by pmeziere on 10/11/22.
//

#ifndef SHMATERIALS_READ_HPP
#define SHMATERIALS_READ_HPP

#include "Helpers.hpp"


class LoadShMat{
public:
    LoadShMat(std::string filename);

    int m_version;
    int m_theta, m_phi;

    typedef struct{
        std::vector<Vector3> coeffs;
        std::vector<Vector3> normalizationCoeffs;
        int numberCoeffs;
    }IncidentDirection;
    std::vector<IncidentDirection> coeffs;

private:
    void read_v1();
    void read_v2();

    FILE *f;

};

class Read {
public:

    enum Type{FULL, SIMPLE, PRECISE, DOZEN};

    Read(std::string filename, Type type, int theta=0, int phi=0);

    void shmat(std::string filename);

    Type m_type;
    int m_theta, m_phi;

};


#endif //SHMATERIALS_READ_HPP
