//
// Created by pmeziere on 10/11/22.
//

#ifndef SHMATERIALS_ARGS_HPP
#define SHMATERIALS_ARGS_HPP

#include <vector>
#include <string>
#include <cstring>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <cstdarg>
#include <functional>
#include <math.h>
#include <cmath>
#include "glmLib/vec3.hpp"

typedef glm::vec3 Vector3;
typedef std::vector<float> VectorFloat;

#define Math_Pi 3.141592653
#define Math_PHI 1.61803399

enum LogType {logError, logDebug, logWarning, logInfo, logSuccess, logNoLabel};

class Log
{
public:
    Log() {}
    Log(LogType type){
        switch(type){
            case logError:
                std::cout << "\033[31m" << "   [Error] ";
                break;
            case logDebug:
                std::cout << "\033[34m" << "   [Debug] ";
                break;
            case logWarning:
                std::cout << "\033[35m" << " [Warning] ";
                break;
            case logInfo:
                std::cout << "\033[37m" << "    [Info] ";
                break;
            case logSuccess:
                std::cout << "\033[32m" << " [Success] ";
                break;
            case logNoLabel:
                std::cout << "           ";
                break;
        }
    }

    ~Log(){
        std::cout << "\033[0m" << std::endl;
    }

    template<class T>
    Log &operator<<(const T &msg){
        std::cout << msg;
        return *this;
    }
};



class Args {
public:
    Args(int argc, char **argv);

    // Useful to play with arguments
    std::string getString(std::string def = "");
    bool getStringEqual(std::string s);
    float getFloat(float def = 0);
    int getInt(int def = 0);
    Vector3 getVector3(Vector3 vec = Vector3(0));
    bool command(std::string com);
    bool argsNumber(std::vector<int> args);

private:
    int m_argc;
    char **m_argv;

    int arg = 1;
};


#endif //SHMATERIALS_ARGS_HPP
