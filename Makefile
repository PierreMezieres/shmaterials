#makefile 
CC=g++
OPT=-Wall -O3 -funroll-loops 
EXEC1=read
ALLEXEC=$(EXEC1)

all: directoryBuild $(ALLEXEC)

build/%.o: Code/%.cpp
	$(CC) -c -o $@ $< $(OPT)

build/%.o: %.cpp
	$(CC) -c -o $@ $< $(OPT)

$(EXEC1): build/main.o build/Helpers.o build/Read.o
	$(CC) -o $@ $^

directoryBuild:
	@mkdir -p build

clean:
	rm -rf build/* build $(ALLEXEC)